package org.thalion.snippet.swagger;

import java.util.Collections;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/*
 * Generic SpringBoot, only configured by sSetting the scanBasePckages restricts the scanned packages
 * 
 * 
 * This is the same as writing down the 3 following annotations
 * @Configuration
 * 
 * @EnableAutoConfiguration
 * 
 * @ComponentScan
 * 
 * @see http://localhost:9090/v2/api-docs?group=api-videogames
 * @see http://localhost:9090/swagger-ui.html
 */
@SpringBootApplication(scanBasePackages = { "org.thalion.snippet.swagger" })
public class SpringbootRunner {

	/**
	 * Just runs the application
	 * @param args
	 */
	public static void main(String[] args) {
		SpringApplication app = new SpringApplication(SpringbootRunner.class);
        app.setDefaultProperties(Collections
          .singletonMap("server.port", "9090"));
        app.run(args);
	}
}
