package org.thalion.snippet.swagger.api;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * 
 * @author Angeles Broullon
 *
 * Swagger configuration for the demo API
 * 
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	private static final String GROUP_NAME = "api-videogames";
	private static final String API_REGEX = "/api.*";
	
	private static final String TITLE = "Videogames REST api POC";
	private static final String DESCRIPTION = "PoC of a REST api, to test both Springboot and Swagger";
	private static final String TERMS_OF_SERVICE = "https://creativecommons.org/licenses/by/4.0/";
	private static final String CONTACT = "thalion@email.com";
	private static final String LICENSE_NAME = "GNU General Public License v3.0";
	private static final String LICENSE_URL = "https://www.gnu.org/licenses/gpl-3.0.en.html";
	private static final String LICENSE_VERSION = "3.0";

	/**
	 * Generates the docket, with the swagger info
	 * @return the docket with the videogames API information
	 */
	@Bean
	public Docket newApi() {
		return new Docket(DocumentationType.SWAGGER_2).groupName(GROUP_NAME).apiInfo(apiVideogames()).select()
				.paths(PathSelectors.regex(API_REGEX)).build();
	}

	/**
	 * Generates the videogames API information
	 * @return the videogames API information
	 */
	private ApiInfo apiVideogames() {
		return new ApiInfoBuilder().title(TITLE).description(DESCRIPTION).termsOfServiceUrl(TERMS_OF_SERVICE)
				.contact(CONTACT).license(LICENSE_NAME).licenseUrl(LICENSE_URL).version(LICENSE_VERSION).build();
	}
}
