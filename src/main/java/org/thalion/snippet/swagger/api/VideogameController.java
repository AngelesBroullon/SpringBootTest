package org.thalion.snippet.swagger.api;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.thalion.snippet.swagger.api.dto.Videogame;
import org.thalion.snippet.swagger.api.service.IVideogameService;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@RestController
@RequestMapping("/api/videogames")
public class VideogameController {

	@Autowired
	private IVideogameService service;

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get Videogames", notes = "Returns all the videogame data")
	@ApiResponses({ @ApiResponse(code = 200, message = "Returns this information") })
	public Collection<Videogame> getAllVideogames() {
		return service.findAll();
	}

	@ResponseStatus(HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, value = "{name}", produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Get the info for one videogame", notes = "Returns the info from one videogame")
	@ApiResponses({ @ApiResponse(code = 200, message = "Exists this information") })
	public Videogame getVideogameByName(
			@ApiParam(defaultValue = "default", value = "The name of the videogame to return") @PathVariable String name) {
		return service.findByName(name);
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	@ApiOperation(value = "Create videogame information", notes = "Create a videogame entry")
	@ApiResponses({ @ApiResponse(code = 201, message = "The videgame entry was created successfully") })
	public void createVideogame(@RequestBody Videogame videogame) {
		service.save(videogame);
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(method = RequestMethod.PUT, value = "{name}")
	@ApiOperation(value = "Update videogame information", notes = "Update a videogame information entry")
	@ApiResponses({ @ApiResponse(code = 204, message = "The videgame entry was updated successfully") })
	public void updateVideogame(
			@ApiParam(defaultValue = "Default", value = "The name of the videogame to update") @PathVariable String name,
			@RequestBody Videogame videogame) {
		videogame.setName(name);
		service.update(videogame);
	}

	@ResponseStatus(HttpStatus.NO_CONTENT)
	@RequestMapping(method = RequestMethod.DELETE, value = "{name}")
	@ApiOperation(value = "Delete videogame", notes = "Deletes a videogame entry")
	@ApiResponses({ @ApiResponse(code = 204, message = "The videgame entry was deleted successfully") })
	public void deleteVideogame(
			@ApiParam(defaultValue = "Default", value = "The name of the videogame to delete") @PathVariable String name) {
		service.delete(name);
	}

}
