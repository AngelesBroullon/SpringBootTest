package org.thalion.snippet.swagger.api.dao;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Repository;
import org.thalion.snippet.swagger.api.dto.Videogame;

/**
 * The Data Access Object implementation
 * 
 * @author Angeles Broullon
 *
 */
@Repository
public class VideogameDAOMocker implements IVideogameDAO {

	/**
	 * The storage for our demo, a Map
	 */
	private Map<String, Videogame> storage;
	
	public VideogameDAOMocker() {
		storage = new HashMap<>();
	}

	/**
	 * Finds all the videogames
	 * 
	 * @return gets all the videogames
	 */
	@Override
	public Collection<Videogame> findAll() {
		return storage.values();
	}

	/**
	 * Finds a videogame by name
	 * 
	 * @param name the name which identifies it
	 * @return
	 */
	@Override
	public Videogame findByName(String name) {
		return storage.get(name);
	}

	/**
	 * Saves the videogame
	 * 
	 * @param videogame the videogame to save
	 */
	@Override
	public void save(Videogame videogame) {
		storage.put(videogame.getName(), videogame);
	}

	/**
	 * Updates the videogame
	 * 
	 * @param videogame the videogame to update
	 */
	@Override
	public void update(Videogame videogame) {
		storage.put(videogame.getName(), videogame);
	}

	/**
	 * Deletes the videogame
	 * 
	 * @param videogame the videogame to delete
	 */
	@Override
	public void delete(String name) {
		storage.remove(name);
	}

}
