package org.thalion.snippet.swagger.api.dao;

import java.util.Collection;

import org.thalion.snippet.swagger.api.dto.Videogame;

/**
 * The Data Access Object specifications
 * 
 * @author Angeles Broullon
 *
 */
public interface IVideogameDAO {

	/**
	 * Finds all the videogames
	 * 
	 * @return gets all the videogames
	 */
	Collection<Videogame> findAll();

	/**
	 * Finds a videogame by name
	 * 
	 * @param name the name which identifies it
	 * @return
	 */
	Videogame findByName(String name);

	/**
	 * Saves the videogame
	 * 
	 * @param videogame the videogame to save
	 */
	void save(Videogame videogame);

	/**
	 * Updates the videogame
	 * 
	 * @param videogame the videogame to update
	 */
	void update(Videogame videogame);

	/**
	 * Deletes the videogame
	 * 
	 * @param videogame the videogame to delete
	 */
	void delete(String name);

}
