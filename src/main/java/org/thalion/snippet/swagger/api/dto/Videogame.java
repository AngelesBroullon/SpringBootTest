package org.thalion.snippet.swagger.api.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 * The data transfer object
 * 
 * @author Angeles Broullon
 * 
 */
@ApiModel(value = "Videogame entity", description = "Complete data of a entity videogame")
public class Videogame {

	/**
	 * The name of the videogame", required = true)
	 */
	@ApiModelProperty(value = "The name of the videogame", required = true)
	private String name;
	/**
	 * The developer of the videogame
	 */
	@ApiModelProperty(value = "The developer of the videogame", required = false)
	private String developer;

	/**
	 * Gets the name of the videogame
	 * 
	 * @return the name of the videogame
	 */
	public String getName() {
		return name;
	}

	/**
	 * Sets the name of the videogame
	 * 
	 * @param name the new name of the videogame
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * Gets the developer of the videogame
	 * 
	 * @return the developer of the videogame
	 */
	public String getDeveloper() {
		return developer;
	}

	/**
	 * Sets the developer of the videogame
	 * 
	 * @param name the new developer of the videogame
	 */
	public void setDeveloper(String developer) {
		this.developer = developer;
	}

}
