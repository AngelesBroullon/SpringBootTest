package org.thalion.snippet.swagger.api.service;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.thalion.snippet.swagger.api.dao.IVideogameDAO;
import org.thalion.snippet.swagger.api.dto.Videogame;

/**
 * The Service implementation
 * 
 * @author Angeles Broullon
 *
 */
@Service
public class VideogameService implements IVideogameService {

	/**
	 * The data access object
	 */
	@Autowired
	private IVideogameDAO dao;

	/**
	 * Finds all the videogames
	 * 
	 * @return gets all the videogames
	 */
	@Override
	public Collection<Videogame> findAll() {
		return dao.findAll();
	}

	/**
	 * Finds a videogame by name
	 * 
	 * @param name the name which identifies it
	 * @return
	 */
	@Override
	public Videogame findByName(String name) {
		return dao.findByName(name);
	}

	/**
	 * Saves the videogame
	 * 
	 * @param videogame the videogame to save
	 */
	@Override
	public void save(Videogame videogame) {
		dao.save(videogame);
	}

	/**
	 * Updates the videogame
	 * 
	 * @param videogame the videogame to update
	 */
	@Override
	public void update(Videogame videogame) {
		dao.update(videogame);
	}

	/**
	 * Deletes the videogame
	 * 
	 * @param videogame the videogame to delete
	 */
	@Override
	public void delete(String name) {
		dao.delete(name);
	}

}
