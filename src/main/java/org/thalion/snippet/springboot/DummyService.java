package org.thalion.snippet.springboot;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * A simple spring boot test, with a 'Hello world'
 * 
 * @author Angeles Broullon Lozano
 *
 */
@RestController
@EnableAutoConfiguration
public class DummyService {

	private static final String WELCOME = "This is a dummy service: hi there!";

	/**
	 * the default configuration is Spring MVC + Tomcat
	 * Gets a welcome message
	 * 
	 * @return a welcome message
	 */
	@RequestMapping("/")
	public String home() {
		return WELCOME;
	}

	public static void main(String[] args) throws Exception {
		// Just boot
		SpringApplication.run(DummyService.class, args);
	}

}
