package org.thalion.snippet.springboot;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = DummyService.class)
public class DummyServiceTest {

	private static final String WELCOME = "This is a dummy service: hi there!";

	private DummyService instance;

	@Before
	public void setUp() {
		instance = new DummyService();
	}

	@Test
	public void home() {
		Assert.assertEquals(WELCOME, instance.home());
	}

	// Integration test for local
	@Test
	public void main() throws Exception {
		String[] args = new String[] { "-Dserver.port=8080" };
		DummyService.main(args);
	}

}
