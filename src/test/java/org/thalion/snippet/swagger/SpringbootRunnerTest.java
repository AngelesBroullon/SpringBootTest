package org.thalion.snippet.swagger;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = SpringbootRunner.class)
public class SpringbootRunnerTest {
	
	@Test
	public void contextLoads() {
	}
	
	// Integration test for local
	@Test(expected = Test.None.class /* no exception expected */)
	public void main() throws Exception {
		String[] args = new String[] { "-Dserver.port=9090"};
		SpringbootRunner.main(args);
	}

}
