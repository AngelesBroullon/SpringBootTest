package org.thalion.snippet.swagger.api.service;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.snippet.swagger.api.dao.IVideogameDAO;
import org.thalion.snippet.swagger.api.dto.Videogame;

@RunWith(MockitoJUnitRunner.class)
public class IVideogameServiceTest {

	private static final String GAME_NAME = "Monkey Island";
	private static final String DEV_NAME = "LucasArts";

	@InjectMocks
	private IVideogameService instance = new VideogameService();

	@Mock
	private IVideogameDAO dao;

	@Mock
	private Videogame videogame;

	@Before
	public void setUp() {
		Mockito.when(dao.findAll()).thenReturn(createDummyListVideogame());
		Mockito.when(dao.findByName(Matchers.anyString())).thenReturn(videogame);

		Mockito.when(videogame.getName()).thenReturn(GAME_NAME);
		Mockito.when(videogame.getDeveloper()).thenReturn(DEV_NAME);
	}

	private Collection<Videogame> createDummyListVideogame() {
		Collection<Videogame> set = new HashSet<>();
		set.add(videogame);
		return set;
	}

	@Test
	public void findAll() {
		Assert.assertEquals(1, instance.findAll().size());
	}

	@Test
	public void findByName() {
		Assert.assertEquals(videogame, instance.findByName(GAME_NAME));
	}

	@Test
	public void save() {
		instance.save(videogame);
		Mockito.verify(dao, Mockito.times(1)).save(Matchers.eq(videogame));
	}

	@Test
	public void update() {
		instance.update(videogame);
		Mockito.verify(dao, Mockito.times(1)).update(Matchers.eq(videogame));
	}

	@Test
	public void delete() {
		instance.delete(GAME_NAME);
		Mockito.verify(dao, Mockito.times(1)).delete(Matchers.eq(GAME_NAME));
	}

}
