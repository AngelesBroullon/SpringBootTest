package org.thalion.snippet.swagger.api;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import springfox.documentation.spring.web.plugins.Docket;

public class SwaggerConfigTest {
	
	private static final String GROUP_NAME = "api-videogames";
	
	private SwaggerConfig instance;
	
	@Before
	public void setUp() {
		instance = new SwaggerConfig();
	}
	
	@Test
	public void newApi() {
		Docket result = instance.newApi();
		Assert.assertNotNull(result);
		Assert.assertEquals(GROUP_NAME, result.getGroupName());		
	}

}
