package org.thalion.snippet.swagger.api;

import java.util.Collection;
import java.util.HashSet;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.runners.MockitoJUnitRunner;
import org.thalion.snippet.swagger.api.dto.Videogame;
import org.thalion.snippet.swagger.api.service.IVideogameService;

@RunWith(MockitoJUnitRunner.class)
public class VideogameControllerTest {
	
	private static final String GAME_NAME = "Monkey Island";
	private static final String DEV_NAME = "LucasArts";

	@InjectMocks
	private VideogameController instance;

	@Mock
	private IVideogameService service;
	
	@Mock
	private Videogame videogame;
	
	@Before
	public void setUp() {
		Mockito.when(service.findAll()).thenReturn(createDummyListVideogame());
		Mockito.when(service.findByName(Matchers.anyString())).thenReturn(videogame);
		
		Mockito.when(videogame.getName()).thenReturn(GAME_NAME);
		Mockito.when(videogame.getDeveloper()).thenReturn(DEV_NAME);
	}
	
	private Collection<Videogame> createDummyListVideogame() {
		Collection<Videogame> set = new HashSet<>();
		set.add(videogame);
		return set;
	}
	
	@Test
	public void getAllVideogames() {
		Assert.assertEquals(1, instance.getAllVideogames().size());
	}
	
	@Test
	public void getVideogameByName() {
		Assert.assertEquals(videogame, instance.getVideogameByName(GAME_NAME));
	}
	
	@Test
	public void createVideogame() {
		instance.createVideogame(videogame);
		Mockito.verify(service, Mockito.times(1)).save(Matchers.eq(videogame));
	}
	
	@Test
	public void updateVideogame() {
		instance.updateVideogame(GAME_NAME, videogame);
		Mockito.verify(service, Mockito.times(1)).update(Matchers.eq(videogame));
	}
	
	@Test
	public void deleteVideogame() {
		instance.deleteVideogame(GAME_NAME);
		Mockito.verify(service, Mockito.times(1)).delete(Matchers.eq(GAME_NAME));
	}

}
