package org.thalion.snippet.swagger.api.dao;

import java.util.Collection;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.thalion.snippet.swagger.api.dto.Videogame;

public class IVideogameDAOTest {

	private static final String GAME_NAME = "Monkey Island";
	private static final String DEV_NAME = "LucasArts";
	private static final String DEV_NAME_2 = "Telltale";

	private IVideogameDAO instance;

	@Before
	public void setUp() {
		instance = new VideogameDAOMocker();
	}

	private Videogame createDummyVideogame() {
		Videogame videogame = new Videogame();
		videogame.setName(GAME_NAME);
		videogame.setDeveloper(DEV_NAME);
		return videogame;
	}

	@Test
	public void findAll() {
		Videogame videogame = createDummyVideogame();
		instance.save(videogame);
		Collection<Videogame> set = instance.findAll();
		Assert.assertEquals(1, set.size());
		Assert.assertTrue(set.contains(videogame));
	}

	@Test
	public void findByName() {
		Videogame videogame = createDummyVideogame();
		instance.save(videogame);
		Assert.assertEquals(videogame, instance.findByName(GAME_NAME));
		Assert.assertEquals(1, instance.findAll().size());
	}

	@Test
	public void save() {
		Videogame videogame = createDummyVideogame();
		instance.save(videogame);
		Assert.assertEquals(videogame, instance.findByName(GAME_NAME));
	}

	@Test
	public void update() {
		Videogame videogame = createDummyVideogame();
		instance.save(videogame);
		Assert.assertEquals(DEV_NAME, instance.findByName(GAME_NAME).getDeveloper());
		videogame.setDeveloper(DEV_NAME_2);		
		instance.update(videogame);
		Assert.assertEquals(DEV_NAME_2, instance.findByName(GAME_NAME).getDeveloper());
	}

	@Test
	public void delete() {
		Videogame videogame = createDummyVideogame();
		instance.save(videogame);
		instance.delete(GAME_NAME);
		Assert.assertNull(instance.findByName(GAME_NAME));
	}

}
