package org.thalion.snippet.swagger.api.dto;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;



public class VideogameTest {
	
	private static final String DEMO_NAME = "Monkey Island";
	private static final String DEV_NAME = "LucasSrts";
	
	private Videogame instance;
	
	@Before
	public void setUp() {
		instance = new Videogame();
	}
	
	@Test
	public void setNameAndgetName() {
		instance.setName(DEMO_NAME);
		Assert.assertEquals(DEMO_NAME, instance.getName());
	}
	
	@Test
	public void setDeveloperAndGetDeveloper() {
		instance.setDeveloper(DEV_NAME);
		Assert.assertEquals(DEV_NAME, instance.getDeveloper());
	}

}
